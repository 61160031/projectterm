package nutchaya;


import java.util.ArrayList;
import tidarat_dao.ProductDao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nutcha1223
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private int amount;
    private String img;
    private String type;
    private boolean status = true;
    
    public Product(int id, String name, double price, int amount, String type, boolean status, String img) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.type = type;
        this.status = status;
        this.img = img;
    }
    public Product(int id, String name, double price, int amount, String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.type = type;
        this.status = status;
        this.img = "1.jpg";
    }

    public Product(int id, String name, double price, String img) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = 0;
        this.type = "เครื่องดื่ม";
        this.status = status;
        this.img = img;
    }

    public Product(int id, String name, double price, int amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.type = "เครื่องดื่ม";
        this.status = status;
        this.img = "1.jpg";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }
    public String getStatus() {
        if (status == true){
            return "true";
        }else {
            return "false";
        } 
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    public int getTotal(){
        return (int) (price*amount);
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", amount=" + amount + ", img=" + img + ", type=" + type + ", status=" + status + '}';
    }


    
    public static ArrayList<Product> genProductList(){
        ProductDao dao = new ProductDao();
        ArrayList<Product> list = dao.getAll();
        /*list.add(new Product(1,"Espresso1",40,"1.jpg"));
        list.add(new Product(2,"Espresso2",30,"1.png"));
        list.add(new Product(3,"Espresso3",40,"1.png"));
        list.add(new Product(4,"Americano1",30,"2.png"));
        list.add(new Product(5,"Americano2",40,"2.png"));
        list.add(new Product(6,"Americano3",50,"2.png"));
        list.add(new Product(7,"ChaYen1",40,"3.png"));
        list.add(new Product(8,"ChaYen2",40,"3.png"));
        list.add(new Product(9,"ChaYen3",40,"3.png"));*/
        return list;
    }
}
