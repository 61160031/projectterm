/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutchaya;

import Satarat_Dao.DaoCustomer;
import Satarat_Model.Customer;
import java.awt.Font;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import static nutchaya.MainPanel.getDate;
import nutchaya.ProductPanel.OnBuyProductListener;
import rujirada.model.Employee;

/**
 *
 * @author Nutcha1223
 */
public class POSPanel extends javax.swing.JPanel implements OnBuyProductListener {

    private ArrayList<Customer> customerList;
    private Customer cus;
    private Employee emp;
    private DaoCustomer daoCus = new DaoCustomer();
    private ArrayList<Product> productList;
    private final ArrayList<Product> selectProduct = new ArrayList();
    private listTableModel model;
    private Date date = getDate();

    private double discount;
    int unitProduct;
    double total;
    double income;
    double change;
    double sumTotal;

    String nameCus;
    String telCus;
    private SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dateString = simpleDate.format(date);

    /**
     * Creates new form ProductMenu
     */
    public POSPanel(Employee emp) {
        //System.out.println("emp id = "+emp.getId());
        //System.out.println("Date: "+dateString);
        initComponents();
        this.emp = emp;
        loadListTableModel();
        btnPay.setEnabled(false);
        refreshPage();

        customerList = daoCus.getAll();
        productList = Product.genProductList();
        int productSize = productList.size();
        menuPanel.setLayout(new GridLayout(productSize / 2 + productSize % 2, 2));

        for (Product product : productList) {
            ProductPanel p = new ProductPanel(product);
            p.addOnBuyProductListener(this);
            menuPanel.add(p);
        }
    }

    private void loadListTableModel() {
        listTableModel table = new listTableModel(selectProduct);
        listProductTable.setModel(table);
    }

    @Override
    public void buy(Product product, int amount) {
        if (selectProduct.isEmpty()) {
            product.setAmount(amount);
            selectProduct.add(product);
        } else {
            if (selectProduct.contains(product)) {
                int indexOfSize = selectProduct.indexOf(product); //ได้ index ของตัวที่ add เข้ามา
                if (amount == 0) {
                    selectProduct.remove(product);
                } else {
                    selectProduct.set(indexOfSize, product).setAmount(amount);
                }
            } else {
                product.setAmount(amount);
                selectProduct.add(product);
            }
        }

        loadSelectProduct();
        for (Product p : selectProduct) {
            System.out.println("Product Panel: " + product + " Amount: " + amount);
            System.out.println("id: " + p.getId());
            System.out.println("Amount: " + p.getAmount());
        }
    }

    public void loadSelectProduct() {
        model = new listTableModel(selectProduct);
        listProductTable.setModel(model);
    }

    private void checkCustomer(String tel) {
        customerList.forEach((Customer m) -> {
            if (m.getPhone().equals(tel)) {
                cus = m;
            }
        });
        if (cus != null) {
            nameCus = cus.getName();
            telCus = cus.getPhone();
            discount = cus.getDiscount();
            lbShowMember.setText("ชื่อ: " + cus.getName());
            txtDiscount.setText("" + cus.getDiscount());
            lbShowTel.setText("เบอร์โทร: " + cus.getPhone());
            btnPay.setEnabled(false);
        } else {
            lbShowMember.setText("not found " + tel);
            txtDiscount.setText("0.0");
            discount = 0;
        }

    }

    public void refreshPage() {
        if (selectProduct != null) {
            selectProduct.clear();
        }
        loadListTableModel();
        setValueDefault();
        btnPay.setEnabled(false);
        System.out.println("Size = " + selectProduct.size());

    }

    public void setValueDefault() {
        cus = null;
        discount = 0;
        unitProduct = 0;
        total = 0;
        income = 0;
        change = 0;
        sumTotal = 0;

        txtAmount.setText("0");
        txtTotalPrice.setText("0.0");
        txtChange.setText("0.0");
        txtDiscount.setText("0.0");
        incomeText.setText("");
        lbShowMember.setText("");
        lbShowTel.setText("");
        txtTotalSum.setText("0.0");
        checkText.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        orderPanel = new javax.swing.JPanel();
        orderTableScl = new javax.swing.JScrollPane();
        listProductTable = new javax.swing.JTable();
        orderCalculatePanel = new javax.swing.JPanel();
        incomeText = new javax.swing.JTextField();
        lbAmount = new javax.swing.JLabel();
        lbTotalPrice = new javax.swing.JLabel();
        lbDiscount = new javax.swing.JLabel();
        lbIncome = new javax.swing.JLabel();
        lbChange = new javax.swing.JLabel();
        lbBaht1 = new javax.swing.JLabel();
        lbBaht2 = new javax.swing.JLabel();
        lbBaht3 = new javax.swing.JLabel();
        lbBaht4 = new javax.swing.JLabel();
        lbUnit = new javax.swing.JLabel();
        btnPay = new javax.swing.JButton();
        btnCalculate = new javax.swing.JButton();
        txtAmount = new javax.swing.JLabel();
        txtTotalPrice = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JLabel();
        txtChange = new javax.swing.JLabel();
        lbTotalSum = new javax.swing.JLabel();
        txtTotalSum = new javax.swing.JLabel();
        lbBaht5 = new javax.swing.JLabel();
        checkMemberPanel = new javax.swing.JPanel();
        lbCheckMember = new javax.swing.JLabel();
        checkText = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        showMemberCheck = new javax.swing.JPanel();
        lbShowMember = new javax.swing.JLabel();
        lbShowTel = new javax.swing.JLabel();
        btnRegister = new javax.swing.JButton();
        menuScroll = new javax.swing.JScrollPane();
        menuPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();

        setBackground(new java.awt.Color(51, 110, 123));
        setMaximumSize(new java.awt.Dimension(609, 469));
        setMinimumSize(new java.awt.Dimension(609, 469));
        setPreferredSize(new java.awt.Dimension(609, 469));

        listProductTable.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        listProductTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Name", "Price", "Amount", "Total"
            }
        ));
        listProductTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        orderTableScl.setViewportView(listProductTable);

        orderCalculatePanel.setBackground(new java.awt.Color(52, 73, 94));

        incomeText.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        incomeText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        incomeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incomeTextActionPerformed(evt);
            }
        });

        lbAmount.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbAmount.setForeground(new java.awt.Color(255, 255, 255));
        lbAmount.setText("Amount");

        lbTotalPrice.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbTotalPrice.setForeground(new java.awt.Color(255, 255, 255));
        lbTotalPrice.setText("Total price");

        lbDiscount.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbDiscount.setForeground(new java.awt.Color(255, 255, 255));
        lbDiscount.setText("Discount");

        lbIncome.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbIncome.setForeground(new java.awt.Color(255, 255, 255));
        lbIncome.setText("Income");

        lbChange.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbChange.setForeground(new java.awt.Color(255, 255, 255));
        lbChange.setText("Change");

        lbBaht1.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbBaht1.setForeground(new java.awt.Color(255, 255, 255));
        lbBaht1.setText("baht");

        lbBaht2.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbBaht2.setForeground(new java.awt.Color(255, 255, 255));
        lbBaht2.setText("baht");

        lbBaht3.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbBaht3.setForeground(new java.awt.Color(255, 255, 255));
        lbBaht3.setText("baht");

        lbBaht4.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbBaht4.setForeground(new java.awt.Color(255, 255, 255));
        lbBaht4.setText("baht");

        lbUnit.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbUnit.setForeground(new java.awt.Color(255, 255, 255));
        lbUnit.setText("Unit");

        btnPay.setBackground(new java.awt.Color(0, 82, 33));
        btnPay.setFont(new java.awt.Font("TH-Chara", 0, 24)); // NOI18N
        btnPay.setForeground(new java.awt.Color(255, 255, 255));
        btnPay.setText("ชำระเงิน");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnCalculate.setBackground(new java.awt.Color(7, 82, 159));
        btnCalculate.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnCalculate.setForeground(new java.awt.Color(255, 255, 255));
        btnCalculate.setText("คำนวน");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        txtAmount.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        txtAmount.setForeground(new java.awt.Color(255, 255, 255));
        txtAmount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtAmount.setText("0");

        txtTotalPrice.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        txtTotalPrice.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalPrice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtTotalPrice.setText("0.0");

        txtDiscount.setBackground(new java.awt.Color(108, 122, 137));
        txtDiscount.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        txtDiscount.setForeground(new java.awt.Color(255, 255, 255));
        txtDiscount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtDiscount.setText("0.0");

        txtChange.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        txtChange.setForeground(new java.awt.Color(255, 255, 255));
        txtChange.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtChange.setText("0.0");

        lbTotalSum.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbTotalSum.setForeground(new java.awt.Color(255, 255, 255));
        lbTotalSum.setText("Summary");

        txtTotalSum.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        txtTotalSum.setForeground(new java.awt.Color(255, 255, 255));
        txtTotalSum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtTotalSum.setText("0.0");

        lbBaht5.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        lbBaht5.setForeground(new java.awt.Color(255, 255, 255));
        lbBaht5.setText("baht");

        javax.swing.GroupLayout orderCalculatePanelLayout = new javax.swing.GroupLayout(orderCalculatePanel);
        orderCalculatePanel.setLayout(orderCalculatePanelLayout);
        orderCalculatePanelLayout.setHorizontalGroup(
            orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(lbAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(lbTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbBaht1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(lbDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbBaht2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(lbIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(incomeText, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbBaht3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(btnCalculate)
                        .addGap(7, 7, 7)
                        .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbChange, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbTotalSum))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTotalSum, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                            .addComponent(txtChange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbBaht5, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                            .addComponent(lbBaht4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(0, 13, Short.MAX_VALUE))
        );
        orderCalculatePanelLayout.setVerticalGroup(
            orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbAmount)
                    .addComponent(txtAmount)
                    .addComponent(lbUnit))
                .addGap(11, 11, 11)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTotalPrice)
                    .addComponent(txtTotalPrice)
                    .addComponent(lbBaht1))
                .addGap(11, 11, 11)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbDiscount)
                    .addComponent(txtDiscount)
                    .addComponent(lbBaht2))
                .addGap(11, 11, 11)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(incomeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbIncome)
                            .addComponent(lbBaht3))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(orderCalculatePanelLayout.createSequentialGroup()
                        .addComponent(lbTotalSum)
                        .addGap(11, 11, 11))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, orderCalculatePanelLayout.createSequentialGroup()
                        .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbBaht4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotalSum))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbBaht5)
                    .addComponent(lbChange)
                    .addComponent(txtChange))
                .addGap(16, 16, 16)
                .addGroup(orderCalculatePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        checkMemberPanel.setBackground(new java.awt.Color(52, 73, 94));
        checkMemberPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbCheckMember.setFont(new java.awt.Font("TH-Chara", 1, 18)); // NOI18N
        lbCheckMember.setForeground(new java.awt.Color(255, 255, 255));
        lbCheckMember.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbCheckMember.setText("Check member");
        checkMemberPanel.add(lbCheckMember, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 190, 20));

        checkText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkTextActionPerformed(evt);
            }
        });
        checkMemberPanel.add(checkText, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 141, 30));

        btnSearch.setBackground(new java.awt.Color(34, 167, 240));
        btnSearch.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("ค้นหา");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        checkMemberPanel.add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, -1, 30));

        showMemberCheck.setBackground(new java.awt.Color(108, 122, 137));

        lbShowMember.setFont(new java.awt.Font("TH-Chara", 0, 24)); // NOI18N
        lbShowMember.setForeground(new java.awt.Color(255, 255, 255));
        lbShowMember.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lbShowTel.setBackground(new java.awt.Color(108, 122, 137));
        lbShowTel.setFont(new java.awt.Font("TH-Chara", 0, 24)); // NOI18N
        lbShowTel.setForeground(new java.awt.Color(255, 255, 255));
        lbShowTel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout showMemberCheckLayout = new javax.swing.GroupLayout(showMemberCheck);
        showMemberCheck.setLayout(showMemberCheckLayout);
        showMemberCheckLayout.setHorizontalGroup(
            showMemberCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showMemberCheckLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(showMemberCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbShowMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbShowTel, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE))
                .addContainerGap())
        );
        showMemberCheckLayout.setVerticalGroup(
            showMemberCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showMemberCheckLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lbShowMember, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbShowTel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        checkMemberPanel.add(showMemberCheck, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 216, 120));

        btnRegister.setBackground(new java.awt.Color(194, 59, 35));
        btnRegister.setFont(new java.awt.Font("TH-Chara", 0, 24)); // NOI18N
        btnRegister.setForeground(new java.awt.Color(255, 255, 255));
        btnRegister.setText("สมัครสมาชิก");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });
        checkMemberPanel.add(btnRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 220, 40));

        javax.swing.GroupLayout orderPanelLayout = new javax.swing.GroupLayout(orderPanel);
        orderPanel.setLayout(orderPanelLayout);
        orderPanelLayout.setHorizontalGroup(
            orderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(orderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(orderTableScl)
                    .addGroup(orderPanelLayout.createSequentialGroup()
                        .addComponent(orderCalculatePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(checkMemberPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        orderPanelLayout.setVerticalGroup(
            orderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orderPanelLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(orderTableScl, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(orderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(checkMemberPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(orderCalculatePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout menuPanelLayout = new javax.swing.GroupLayout(menuPanel);
        menuPanel.setLayout(menuPanelLayout);
        menuPanelLayout.setHorizontalGroup(
            menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 583, Short.MAX_VALUE)
        );
        menuPanelLayout.setVerticalGroup(
            menuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 734, Short.MAX_VALUE)
        );

        menuScroll.setViewportView(menuPanel);

        jPanel1.setBackground(new java.awt.Color(52, 73, 94));

        lbTitle.setFont(new java.awt.Font("TH-Chara", 1, 24)); // NOI18N
        lbTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("หน้าขาย");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lbTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 7, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(orderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(menuScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(orderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(menuScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void checkTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkTextActionPerformed

    }//GEN-LAST:event_checkTextActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        String tel = checkText.getText();
        checkCustomer(tel);

    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        System.out.println("discount: " + discount + " sum: " + sumTotal + " income: " + income + " change: " + change);
        ReceiptPanel orderSelectProduct = new ReceiptPanel(this, emp, discount, sumTotal, income, change, cus, dateString, selectProduct);
        orderSelectProduct.setVisible(true);
        orderSelectProduct.setLocationRelativeTo(null);
        cus = null;
    }//GEN-LAST:event_btnPayActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        int unit = 0;
        double totalPrice = 0;
        double incomeValue = 0;
        double changeValue = 0;
        double realTotalPrice = 0;
        for (Product p : selectProduct) {
            unit += p.getAmount();
            totalPrice += p.getTotal();
        }
        realTotalPrice = totalPrice - discount;
        
        txtTotalPrice.setText("" + totalPrice);
        txtTotalSum.setText("" + realTotalPrice);
        txtAmount.setText("" + unit);

        unitProduct = unit;
        /*total = totalPrice;
        income = incomeValue;
        change = changeValue;
        sumTotal = realTotalPrice;*/

        checkCalculate(incomeValue, realTotalPrice, totalPrice, changeValue);
    }//GEN-LAST:event_btnCalculateActionPerformed

    public void setbtnPay(double incomeValue, double changeValue, double totalPrice) {
        if (incomeText.getText().isEmpty() || changeValue < 0 || totalPrice == 0) {
            btnPay.setEnabled(false);
            txtChange.setText("0.0");
        } else {
            btnPay.setEnabled(true);
            txtChange.setText("" + changeValue);
        }
    }

    public void checkCalculate(double incomeValue, double realTotalPrice, double totalPrice, double changeValue) {
        if (!incomeText.getText().isEmpty()) {
            incomeValue = Integer.valueOf(incomeText.getText());
            //realTotalPrice = totalPrice - discount;
            changeValue = incomeValue - realTotalPrice;
            if (changeValue >= 0) {
                txtChange.setText("" + changeValue);
                txtTotalSum.setText("" + realTotalPrice);
            } else {
                txtChange.setText("0.0");
                btnPay.setEnabled(false);
            }
        } else {
            txtChange.setText("0.0");
        }
        total = realTotalPrice;
        income = incomeValue;
        change = changeValue;
        sumTotal = realTotalPrice;
        System.out.println("discount: " + discount + " sum: " + sumTotal + " income: " + income + " change: " + change);
        setbtnPay(incomeValue, changeValue, totalPrice);
    }

    private void incomeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_incomeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_incomeTextActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed

        RegisterPanel regis = new RegisterPanel();
        regis.setVisible(true);
        regis.setLocationRelativeTo(null);
    }//GEN-LAST:event_btnRegisterActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnSearch;
    private javax.swing.JPanel checkMemberPanel;
    private javax.swing.JTextField checkText;
    private javax.swing.JTextField incomeText;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbAmount;
    private javax.swing.JLabel lbBaht1;
    private javax.swing.JLabel lbBaht2;
    private javax.swing.JLabel lbBaht3;
    private javax.swing.JLabel lbBaht4;
    private javax.swing.JLabel lbBaht5;
    private javax.swing.JLabel lbChange;
    private javax.swing.JLabel lbCheckMember;
    private javax.swing.JLabel lbDiscount;
    private javax.swing.JLabel lbIncome;
    private javax.swing.JLabel lbShowMember;
    private javax.swing.JLabel lbShowTel;
    private javax.swing.JLabel lbTitle;
    private javax.swing.JLabel lbTotalPrice;
    private javax.swing.JLabel lbTotalSum;
    private javax.swing.JLabel lbUnit;
    private javax.swing.JTable listProductTable;
    private javax.swing.JPanel menuPanel;
    private javax.swing.JScrollPane menuScroll;
    private javax.swing.JPanel orderCalculatePanel;
    private javax.swing.JPanel orderPanel;
    private javax.swing.JScrollPane orderTableScl;
    private javax.swing.JPanel showMemberCheck;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JLabel txtChange;
    private javax.swing.JLabel txtDiscount;
    private javax.swing.JLabel txtTotalPrice;
    private javax.swing.JLabel txtTotalSum;
    // End of variables declaration//GEN-END:variables

    private class listTableModel extends AbstractTableModel {

        private ArrayList<Product> dataProduct;
        String columnName[] = {"No.", "Name", "Price", "Amount", "Total Price"};

        private listTableModel(ArrayList<Product> selectProduct) {
            this.dataProduct = selectProduct;
        }

        @Override
        public int getRowCount() {
            return this.dataProduct.size();
        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Product product = this.dataProduct.get(rowIndex);
            if (columnIndex == 0) {
                return rowIndex + 1;
            }
            if (columnIndex == 1) {
                return product.getName();
            }
            if (columnIndex == 2) {
                return product.getPrice();
            }
            if (columnIndex == 3) {
                return product.getAmount();
            }
            if (columnIndex == 4) {
                return product.getTotal();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columnName[column];
        }

    }

}
