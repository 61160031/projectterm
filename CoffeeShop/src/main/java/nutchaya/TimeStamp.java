/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutchaya;

import java.text.SimpleDateFormat;

/**
 *
 * @author Nutcha1223
 */
public class TimeStamp {
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private int id;
    private int empId;
    private String empName;
    private int salId;
    private String timeIn;
    private String timeOut;

    public TimeStamp(int id, int empId, String empName, int salId, String timeIn, String timeOut) {
        this.id = id;
        this.empId = empId;
        this.empName = empName;
        this.salId = salId;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }
    
    public TimeStamp(int id, int empId, String empName, int salId) {
        this.id = id;
        this.empId = empId;
        this.empName = empName;
        this.salId = salId;
        this.timeIn = simpleDateFormat.format(new java.util.Date());;
        this.timeOut = simpleDateFormat.format(new java.util.Date());;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }
    
    public int getSalId() {
        return salId;
    }

    public void setSalId(int salId) {
        this.salId = salId;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut() {
        this.timeOut = simpleDateFormat.format(new java.util.Date());;
    }

    @Override
    public String toString() {
        return "TimeStamp{" +  "id=" + id + ", empId=" + empId + ", salId=" + salId + ", timeIn=" + timeIn + ", timeOut=" + timeOut + '}';
    }
    
}
