/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutchaya;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class TestSelectTimeStamp {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT time_stamp_id,\n" +
                        "       time_stamp.emp_id, emp_name,\n" +
                        "       sal_id,\n" +
                        "       time_stamp_in,\n" +
                        "       time_stamp_out\n" +
                        "  FROM time_stamp\n" +
                        "  INNER JOIN employee ON time_stamp.emp_id = employee.emp_id WHERE time_stamp_id=1;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("time_stamp_id");
                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_name");
                int salId = result.getInt("sal_id");
                String timeIn = result.getString("time_stamp_in");
                String timeOut = result.getString("time_stamp_out");

                TimeStamp timeStamp = new TimeStamp(id, empId, empName, salId, timeIn, timeOut);
                System.out.println(timeStamp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectTimeStamp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
