/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutchaya;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import tidarat_dao.DaoInterface;
import tidarat.TestSelectProduct;

/**
 *
 * @author Nutcha1223
 */
public class TimeStampDao implements DaoInterface<TimeStamp> {

    @Override
    public int add(TimeStamp object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO time_stamp (emp_id,sal_id,\n" +
"                       time_stamp_in,\n" +
"                       time_stamp_out)                       \n" +
"                       VALUES (?,?,?,?);";
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpId());
            stmt.setInt(2, object.getSalId());
            stmt.setString(3, object.getTimeIn());
            stmt.setString(4, object.getTimeOut());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<TimeStamp> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
           String sql = "SELECT time_stamp_id,\n" +
                        "       time_stamp.emp_id, emp_name,\n" +
                        "       sal_id,\n" +
                        "       time_stamp_in,\n" +
                        "       time_stamp_out\n" +
                        "  FROM time_stamp\n" +
                        "  INNER JOIN employee ON time_stamp.emp_id = employee.emp_id;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("time_stamp_id");
                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_name");
                int salId = result.getInt("sal_id");
                String timeIn = result.getString("time_stamp_in");
                String timeOut = result.getString("time_stamp_out");

                TimeStamp timeStamp = new TimeStamp(id, empId, empName, salId, timeIn, timeOut);
                System.out.println(timeStamp);
                list.add(timeStamp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectTimeStamp.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public TimeStamp get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
         try {
            String sql = "SELECT time_stamp_id,\n" +
                        "       time_stamp.emp_id, emp_name,\n" +
                        "       sal_id,\n" +
                        "       time_stamp_in,\n" +
                        "       time_stamp_out\n" +
                        "  FROM time_stamp\n" +
                        "  INNER JOIN employee ON time_stamp.emp_id = employee.emp_id WHREE time_stamp_id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int tid = result.getInt("time_stamp_id");
                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_name");
                int salId = result.getInt("sal_id");
                String timeIn = result.getString("time_stamp_in");
                String timeOut = result.getString("time_stamp_out");

                TimeStamp timeStamp = new TimeStamp(id, empId, empName, salId, timeIn, timeOut);
                System.out.println(timeStamp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectTimeStamp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(TimeStamp object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
