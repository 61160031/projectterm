/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutthaporn_dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import nutthaporn_model.Promotion_Model;

/**
 *
 * @author user
 */
public class PromotionDao implements DaoInterface<Promotion_Model> {

    @Override
    public int add(Promotion_Model obj) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO promotion (pro_name,pro_start_date,pro_end_date,pro_status)VALUES(?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            //System.out.println("Name");
            stmt.setString(2, obj.getStart());
            //System.out.println("Start");
            stmt.setString(3, obj.getEnd());
            //System.out.println("End");
            stmt.setString(4, "" + obj.isStatus());
            //System.out.println("Status");
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            //System.out.println("Affect row "+row+" id: "+id);

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    @Override
    public ArrayList<Promotion_Model> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM promotion;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("pro_id");
                String name = result.getString("pro_name");
                String start = result.getString("pro_start_date");
                String end = result.getString("pro_end_date");
                String statusStr = result.getString("pro_status");
                boolean status = true;
                if (statusStr.equals("true")) {
                    status = true;
                } else {
                    status = false;
                }
                Promotion_Model promotion = new Promotion_Model(id, name, start, end, status);
                list.add(promotion);
                System.out.println(promotion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Promotion_Model get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT pro_id,pro_name,pro_start_date,pro_end_date,pro_status FROM promotion WHERE pro_id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int proid = result.getInt("pro_id");
                String name = result.getString("pro_name");
                String start = result.getString("pro_start_date");
                String end = result.getString("pro_end_date");
                String statusStr = result.getString("pro_status");
                boolean status = true;
                if (statusStr.equals("true")) {
                    status = true;
                } else {
                    status = false;
                }
                Promotion_Model promotion = new Promotion_Model(id, name, start, end, status);
                return promotion;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM promotion WHERE pro_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Promotion_Model obj) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE promotion SET pro_name = ?,pro_start_date = ?,pro_end_date = ?,pro_status = ? WHERE pro_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getStart());
            stmt.setString(3, obj.getEnd());
            stmt.setString(4, ""+obj.isStatus());
            stmt.setInt(5, obj.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {

        PromotionDao dao = new PromotionDao();
        System.out.println(dao.get(1).isStatus());

        System.out.println(dao.getAll());
        //System.out.println(dao.get(1));
       // int id = dao.add(new Promotion_Model(-1, "1 แถม 1", "2020-11-01", "2020-12-31"));
       // System.out.println("id: " + id);
        //System.out.println(dao.get(id));
        
        //Promotion_Model lastPromotion = dao.get(3);
        //System.out.println("Last Promotion: " + lastPromotion);
        //lastPromotion.setStatus(true);
        //dao.update(lastPromotion);
        
        //int row = dao.update(lastPromotion);
        
        //Promotion_Model updatePromostion = dao.get(6);
        //System.out.println("Update Promotion: " + updatePromostion);
        
        //dao.delete(7);
        //Promotion_Model deletePromotion = dao.get(7);
        //System.out.println("Delete Promotion: " + deletePromotion);
    }

}
