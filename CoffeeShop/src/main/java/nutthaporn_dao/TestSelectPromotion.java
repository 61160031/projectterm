/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutthaporn_dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import nutthaporn_model.Promotion_Model;

/**
 *
 * @author user
 */
public class TestSelectPromotion {
    public static void main(String[] args){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT pro_id,pro_name,pro_start_date,pro_end_date,pro_status FROM promotion";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("pro_id");
                String name = result.getString("pro_name");
                String start = result.getString("pro_start_date");
                String end = result.getString("pro_end_date"); 
                String statusStr = result.getString("pro_status"); 
                boolean status = true;
                if(statusStr.equals("true")){
                    status = true;
                }else{ 
                    status = false;
                }
                Promotion_Model promotion = new Promotion_Model(id,name,start,end,status);
                System.out.println(promotion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
}
