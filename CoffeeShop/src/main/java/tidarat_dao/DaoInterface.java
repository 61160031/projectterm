package tidarat_dao;

import java.util.ArrayList;

/**
 *
 * @author werapan
 */
public interface DaoInterface<T> {
    public int add(T object);
    public ArrayList<T> getAll();
    public T get(int id);
    public int delete(int id);
    public int update(T object);
}
