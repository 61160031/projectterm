/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tidarat_dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import nutchaya.Product;
import tidarat.TestSelectProduct;

/**
 *
 * @author werapan
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (product_name, product_price, product_qty, product_type, product_status, product_img) VALUES (?, ?, ?, ?, ?, ?)";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            stmt.setString(4, object.getType());
            stmt.setString(5, object.getStatus());
            stmt.setString(6, object.getImg());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT product_id, product_name, product_price, product_qty, product_type, product_status, product_img FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                int amount = result.getInt("product_qty");              //
                String type = result.getString("product_type");          //
                String statusStr = result.getString("product_status");       //
                boolean status = true;
                if(statusStr.equals("true")){
                    status = true;
                }else{ 
                    status = false;
                }
                String img = result.getString("product_img");
                Product product = new Product(id, name, price, amount, type, status, img);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT product_id, product_name, product_price, product_qty, product_type,product_status, product_img FROM product WHERE product_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                int amount = result.getInt("product_qty");              //
                String type = result.getString("product_type");          //
                String statusStr = result.getString("product_status");       //
                boolean status = true;
                if(statusStr.equals("true")){
                    status = true;
                }else{ 
                    status = false;
                }
                String img = result.getString("product_img");
                Product product = new Product(id, name, price, amount, type, status, img);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM product WHERE product_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET product_name = ?,product_price = ?,product_qty = ?,product_type = ?,product_status = ? WHERE product_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            stmt.setString(4, object.getType());
            if(object.isStatus()== true){
              stmt.setString(5, "true");  
            }else{
                stmt.setString(5, "false"); 
            }
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        
    }

    
}
