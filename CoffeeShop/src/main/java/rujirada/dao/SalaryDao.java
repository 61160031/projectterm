/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rujirada.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import rujirada.model.Employee;
import rujirada.model.Salary;

/**
 *
 * @author cherr
 */
public class SalaryDao{

    public ArrayList<Salary> getAll(Employee employee) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        ArrayList<Salary> salaryList = new ArrayList<>();
        //select
        try {
            String sql = "SELECT sal_id,emp_id,sal_date,sal_total,sal_status FROM salary WHERE emp_id="+employee.getId();
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int salID = result.getInt("sal_id");
                String date = result.getString("sal_date");
                Double total = result.getDouble("sal_total");
                boolean status = Boolean.parseBoolean(result.getString("sal_status"));
                Salary salary = new Salary(salID, date, employee, status, total);
                salaryList.add(salary);
            }
            return salaryList;
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return null;
    }
    
    public int add(Salary object){
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int id=-1;
        //insert
        try {
            String sql = "INSERT INTO salary (emp_id,sal_date,sal_total,sal_status)VALUES (?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpolyee().getId());
            stmt.setString(2, object.getDate());
            stmt.setDouble(3, object.getTotal());
            stmt.setString(4, ""+object.isStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row "+row+" id: "+id);
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return id;
    }
    
    public Salary get(int id) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        EmployeeDao empDao = new EmployeeDao();
        //select
        try {
            String sql = "SELECT sal_id,emp_id,sal_date,sal_total,sal_status FROM salary WHERE sal_id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int salID = result.getInt("sal_id");
                int empID = result.getInt("emp_id");
                String date = result.getString("sal_date");
                Double total = result.getDouble("sal_total");
                boolean status = Boolean.parseBoolean(result.getString("sal_status"));
                Salary salary = new Salary(salID, date, empDao.get(empID), status, total);
                return salary;
             }
         }catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return null;
    }
    
    public int update(Salary object){
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int row=0;
        try {
            String sql = "UPDATE salary SET sal_date = ?,sal_total = ?,sal_status = ? WHERE sal_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getDate());
            stmt.setDouble(2, object.getTotal());
            stmt.setString(3, ""+object.isStatus());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public int delete(int id){
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM salary WHERE sal_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) { 
            Logger.getLogger(SalaryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }
    public static void main(String[] args) {
        SalaryDao salDao = new SalaryDao();
        EmployeeDao empDao = new EmployeeDao();
        ArrayList<Salary> salary = salDao.getAll(empDao.get(1));
        System.out.println(salary);
        
//        Salary sal = new Salary("2020-11-01", empDao.get(1), false, 10000);
//        int id =salDao.add(sal);
//        System.out.println("New Id : "+id);

        Salary sal1 = salDao.get(7);
        System.out.println(sal1);
        
        sal1.setDate("2020-11-02");
        sal1.setTotal(5000);
        salDao.update(sal1);
        System.out.println("New : "+sal1);
        
//        salDao.delete(2);
//        System.out.println(salDao.get(2));
    }
    
}
