/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rujirada.dao;

import java.util.ArrayList;

/**
 *
 * @author cherr
 */
public interface Dao<T> {
    public int add(T object);
    public ArrayList<T> getAll();
    public T get(int id);
    public int update(T object);
}
