/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rujirada.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import rujirada.model.Employee;

/**
 *
 * @author cherr
 */
public class EmployeeDao implements Dao<Employee>{

    @Override
    public int add(Employee object) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int id=-1;
        //insert
        try {
            String sql = "INSERT INTO employee (emp_name,emp_username,emp_password,"
                    + "emp_tel,emp_type,emp_start_date,emp_status)VALUES (?,?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getType());
            stmt.setString(6, object.getStartDate());
            stmt.setString(7, ""+object.isStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row "+row+" id: "+id);
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return id;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        
        //select
        try {
            String sql = "SELECT emp_id,emp_name,emp_username,emp_password,emp_tel,"
                    + "emp_type,emp_start_date,emp_status FROM employee WHERE emp_id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int empId = result.getInt("emp_id");
                String name = result.getString("emp_name");
                String username,password;
                if(result.getString("emp_username")==null){
                    username = "";
                }else{
                    username = result.getString("emp_username");
                }
                if(result.getString("emp_password")==null){
                    password = "";
                }else{
                    password = result.getString("emp_password");
                }
                String tel = result.getString("emp_tel");
                String type = result.getString("emp_type");
                String startDate = result.getString("emp_start_date");
                boolean status = Boolean.parseBoolean(result.getString("emp_status"));
                Employee employee =new Employee(empId, name, username, password, tel, type, startDate,status);
                return employee;
            }
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return null;
    }
    
    public ArrayList<Employee> getAll(){
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        ArrayList<Employee> list = new ArrayList<>();
        //select
        try {
            String sql = "SELECT emp_id,emp_name,emp_username,emp_password,emp_tel,emp_type,emp_start_date,emp_status FROM employee;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("emp_id");
                String name = result.getString("emp_name");
                String username,password;
                if(result.getString("emp_username")==null){
                    username = "";
                }else{
                    username = result.getString("emp_username");
                }
                if(result.getString("emp_password")==null){
                    password = "";
                }else{
                    password = result.getString("emp_password");
                }
                String tel = result.getString("emp_tel");
                String type = result.getString("emp_type");
                String startDate = result.getString("emp_start_date");
                boolean status = Boolean.parseBoolean(result.getString("emp_status"));
                Employee employee =new Employee(id, name, username, password, tel, type, startDate,status);
                list.add(employee);
            }
        } catch (SQLException ex) { 
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    @Override
    public int update(Employee object) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int row=0;
        try {
            String sql = "UPDATE employee SET emp_name = ?,"
                    + "emp_username = ?,emp_password = ?,"
                    + "emp_tel = ?,emp_type = ?,"
                    + "emp_start_date = ?,emp_status = ? "
                    + "WHERE emp_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getType());
            stmt.setString(6, object.getStartDate());
            stmt.setString(7, ""+object.isStatus());
            stmt.setInt(8, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) { 
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        //add
        Employee emp1 = new Employee("Cherr","0614619568", "พนักงาน", "2020-11-01");
        int id = dao.add(emp1);
        System.out.println("id : "+id);

        //getAll
        System.out.println(dao.getAll());
        //get
        System.out.println(dao.get(1));
        
        //update
        Employee emp2 = dao.get(1);
        emp2.setName("rryn,.");
        emp2.setStatus(false);
        emp2.setPassword("XXXXXXXX");
        dao.update(emp2);
        System.out.println("New update : "+dao.get(1));
    }
}
