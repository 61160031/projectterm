/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rujirada.model;

import java.util.Date;

/**
 *
 * @author cherr
 */
public class Salary {
    private int id;
    private String date;
    private Employee empolyee;
    private boolean status;
    private double total;

    public Salary(int id, String date, Employee empolyee, boolean status,  double total) {
        this.id = id;
        this.date = date;
        this.empolyee = empolyee;
        this.status = status;
        this.total = total;
    }
    
    public Salary(String date, Employee empolyee, boolean status,  double total) {
        this(-1, date, empolyee, status, total);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Employee getEmpolyee() {
        return empolyee;
    }

    public void setEmpolyee(Employee empolyee) {
        this.empolyee = empolyee;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", empolyee=" + empolyee + ", status=" + status + ", total=" + total + '}';
    }
    
    
}
