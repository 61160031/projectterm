/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rujirada.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author cherr
 */
public class Employee {
    private int id;
    private String name;
    private String username;
    private String password;
    private String tel;
    private String type;
    private String startDate;
    private boolean status;
//    private ArrayList<Salary> salary;

    public Employee(int id, String name, String username, String password, String tel, String type, String startDate,boolean status) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.type = type;
        this.startDate = startDate;
        this.status=status;
//        salary = new ArrayList<>();
    }

    public Employee(String name, String tel, String type,String startDate) {
        this(-1, name, null, null, tel, type, startDate,true);
    }
    
    public Employee(String name, String username, String password,String tel, String type, String startDate) {
        this(-1, name, username, password, tel, type, startDate,true);
    }
    
    public Employee(String name, String username, String password,String tel, String type, String startDate,boolean status) {
        this(-1, name, username, password, tel, type, startDate,status);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", username=" + username 
                + ", password=" + password + ", tel=" + tel + ", type=" + type + 
                ", startDate=" + startDate + ", status=" + status + '}';
    }
    
    
}
