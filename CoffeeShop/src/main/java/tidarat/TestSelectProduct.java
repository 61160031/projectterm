/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tidarat;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import nutchaya.Product;

/**
 *
 * @author werapan
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT product_id, product_name, product_price, product_qty, product_type, product_status, product_img FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                int amount = result.getInt("product_qty");              //
                String type = result.getString("product_type");          //
                String statusStr = result.getString("product_status");       //
                boolean status = true;
                if(statusStr.equals("true")){
                    status = true;
                }else{ 
                    status = false;
                }
                String img = result.getString("product_img");
                Product product = new Product(id, name, price, amount, type, status, img);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
