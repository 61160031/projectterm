/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutthaporn_model;

/**
 *
 * @author user
 */
public class Promotion_Model {
    private int id;
    private String name;
    private String start;
    private String end;
    private boolean status;

    public Promotion_Model(int id, String name, String start, String end, boolean status) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.status = status;
    }

    public Promotion_Model(int id, String name, String start, String end) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isStatus() {
        return status;
    }
    
    //public String getStatus() {
      //  if (status == true){
        //    return "true";
        //}else {
          //  return "false";
        //} 
    //}

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Promotion_Model{" + "id=" + id + ", name=" + name + ", start=" + start + ", end=" + end + ", status=" + status + '}';
    }
    
}
