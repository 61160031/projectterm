/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satarat_Dao;

import java.util.ArrayList;
import Satarat_Model.Customer;
import database.Database;
import java.sql.*;

/**
 *
 * @author lovel
 */
public class DaoCustomer implements DaoInterface<Customer> {

    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (cus_name,cus_tel,cus_discount,cus_status) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setDouble(3, object.getDiscount());
            stmt.setString(4, "" + object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (Exception ex) {
            System.out.println("Error insert SQL" + ex);
        }
        db.close();
        return id;
    }

    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {

            String sql = "SELECT cus_id,cus_name,cus_tel, cus_discount,cus_status FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("cus_id");
                String name = result.getString("cus_name");
                String phone = result.getString("cus_tel");
                double discount = result.getDouble("cus_discount");
                String status = result.getString("cus_status");
                boolean statuS = Boolean.parseBoolean(result.getString("cus_status"));

                Customer cus = new Customer(id, name, phone, discount, statuS);
                list.add(cus);
                System.out.println("ID : " + id + " Name : " + name + " Phone : "
                        + phone + " discount : " + discount + " Status : " + status);
            }
        } catch (Exception ex) {
            System.out.println("Error SQL");
        }
        db.close();
        return list;
    }

    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT cus_id,cus_name,cus_tel, cus_discount,cus_status FROM customer WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {

                int Customerid = result.getInt("cus_id");
                String Customername = result.getString("cus_name");
                String Customertel = result.getString("cus_tel");
                double Customerdis = result.getDouble("cus_discount");
                String CustomerSta = result.getString("cus_status");
                boolean statuS = Boolean.parseBoolean(result.getString("cus_status"));

                Customer customer = new Customer(Customerid, Customername,
                        Customertel, Customerdis, statuS);
                return customer;

            }
        } catch (SQLException ex) {
            System.out.println("Get Customer Error");
        }
        return null;
    }

    public int delete(int id) {
        return 0;
    }

    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET cus_name = ?,cus_tel = ?,cus_discount = ?,cus_status = ? WHERE cus_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setDouble(3, object.getDiscount());
            stmt.setString(4, "" + object.getStatus());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("Update Error");
        }
        db.close();
        return row;
    }
}
