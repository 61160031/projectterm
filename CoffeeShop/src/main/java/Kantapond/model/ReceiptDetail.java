/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kantapond.model;

import nutchaya.Product;

/**
 *
 * @author gunm2
 */
public class ReceiptDetail {

    private int id;
    private Product product;
    private double price;
    private int amount;
    Receipt receipt;

    public ReceiptDetail(int id, Receipt receipt, Product product, double price, int amount) {
        this.id = id;
        this.product = product;
        this.price = price;
        this.amount = amount;
        this.receipt = receipt;
    }

    public ReceiptDetail(Receipt receipt, Product product, double price, int amount) {
        this(-1, receipt, product, price, amount);
    }

    public double getTotalDetail() {
        return price * amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public void addAmount(double amout) {
        this.amount += amount;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id
                + ", product=" + product
                + ", price=" + price
                + ", amount=" + amount
                + "total : " + this.getTotalDetail()
                + '}';
    }

}
