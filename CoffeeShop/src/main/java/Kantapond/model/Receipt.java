/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kantapond.model;

import Satarat_Model.Customer;
import java.sql.Time;
import java.text.SimpleDateFormat;
import nutchaya.Product;
import rujirada.model.Employee;
import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author gunm2
 */
public class Receipt {

    private int id;
    private Customer customer;
    private Employee employee;
    private Date date;
    private double total;
    private double discount;
    private double income;
    private double change;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int id, Customer customer, Employee employee, Date date, double total, double discount, double income, double change) {
        this.id = id;
        this.customer = customer;
        this.employee = employee;
        this.date = date;
        this.total = total;
        this.discount = discount;
        this.income = income;
        this.change = change;
        receiptDetail = new ArrayList<>();
    }
    
    public Receipt( Customer customer, Employee employee, Date date, double total, double discount, double income, double change) {
        this.id = -1;
        this.customer = customer;
        this.employee = employee;
        this.date = date;
        this.total = total;
        this.discount = discount;
        this.income = income;
        this.change = change;
        receiptDetail = new ArrayList<>();
    }

    public Receipt(Customer customer, Employee employee) {
        this.customer = customer;
        this.employee = employee;
        receiptDetail = new ArrayList<>();
    }

    public void addReceiptDetail(int id, Product product, double price, int amount) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(id, this, product, price, amount));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(this.id, product, product.getPrice(), amount);
    }

    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);
    }

    public double getTotal() {
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id
                + ", date=" + date
                + ", customer=" + customer
                + ", employee=" + employee
                + ", total=" + total
                + ", discount=" + discount
                + ", income=" + income
                + ", change=" + change + "}\n";
        for (ReceiptDetail r : receiptDetail) {
            str += r.toString() + "\n";
        }
        return str;
    }

}
