/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kantapond.dao;

import Kantapond.model.Receipt;
import Kantapond.model.ReceiptDetail;
import Satarat_Model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import nutchaya.Product;
import rujirada.model.Employee;

/**
 *
 * @author gunm2
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        //insert
        try {
            String sql = "INSERT INTO receipt (cus_id,emp_id,receipt_total_price, receipt_discount,receipt_income,receipt_change) VALUES (?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            if (object.getCustomer() == null) {
                stmt.setInt(1, 0);
            } else {
                stmt.setInt(1, object.getCustomer().getId());
            }
            stmt.setInt(2, object.getEmployee().getId());
            stmt.setDouble(3, object.getTotal());
            stmt.setDouble(4, object.getDiscount());
            stmt.setDouble(5, object.getIncome());
            stmt.setDouble(6, object.getChange());
            int row = stmt.executeUpdate();
            //System.out.println("row: "+row);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                //System.out.println("id: "+id);
//                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,product_id,redetail_price, redetail_amount) VALUES (?,?,?,?)";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, id);
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getTotalDetail());
                stmtDetail.setDouble(4, r.getAmount());
                int rowDetai = stmtDetail.executeUpdate();
                ResultSet resultDetai = stmt.getGeneratedKeys();
                if (resultDetai.next()) {
                    int rid = resultDetai.getInt(1);
                    r.setId(rid);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error : to create receipt" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        ArrayList<Receipt> list = new ArrayList<>();
        //select
        try {
            String sql = "SELECT receipt_id,cus_id,emp_id,receipt_date,receipt_total_price,receipt_discount,receipt_income,receipt_change\n"
                    + "                    FROM receipt\n"
                    + "                    order by receipt_date desc;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("receipt_id");
                int customerId = result.getInt("cus_id");
                int empId = result.getInt("emp_id");
//                String empType = result.getString("emp_type");
//                String empStartDate = result.getString("emp_start_date");
                java.util.Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("receipt_date"));
                double discount = result.getDouble("receipt_discount");
                double total = result.getDouble("receipt_total_price");
                double incom = result.getDouble("receipt_income");
                double change = result.getDouble("receipt_change");
                Receipt receipt = new Receipt(id, new Customer(customerId, "", ""), new Employee(empId, "", "", "", "", "", "", true), created, total,discount , incom, change);
                list.add(receipt);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt!!!" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();

        //select
        try {
            String sql = "SELECT receipt_id,\n"
                    + "       cus_id,\n"
                    + "       r.emp_id as emp_id,\n"
                    + "       emp.emp_name as emp_name,\n"
                    + "       receipt_date,\n"
                    + "       receipt_total_price,\n"
                    + "       receipt_discount,\n"
                    + "       receipt_income,\n"
                    + "       receipt_change \n"
                    + "FROM receipt r,employee emp\n"
                    + "WHERE r.receipt_id =? and r.emp_id = emp.emp_id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("receipt_id");

                int customerId = result.getInt("cus_id");

                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_name");

                java.util.Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("receipt_date"));

                double total = result.getDouble("receipt_total_price");
                double discount = result.getDouble("receipt_discount");
                double incom = result.getDouble("receipt_income");
                double change = result.getDouble("receipt_change");
                Receipt receipt = new Receipt(id, new Customer(customerId, "", ""), new Employee(empName, "", "", ""), created, total, discount, incom, change);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select  receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing  receipt!!!" + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        database.Database db = database.Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //insert
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to Delete id " + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "ช็อคโกแลต ร้อน", 30, 1);
        Product p2 = new Product(1, "ช็อคโกแลต เย็น", 35, 2);
        Customer cus = new Customer(1, "Yaru", "0852546851");
        Employee emp = new Employee(6, "นาดาว", "emp007", "01234567", "08696986985", "พนักงาน", "2020-11-11", true);
        Receipt receipt = new Receipt(cus, emp);
        receipt.addReceiptDetail(p1, 2);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);

        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println("GetAll --> " + dao.getAll());
//        System.out.println("Get --> " + dao.get(1));

    }
}
