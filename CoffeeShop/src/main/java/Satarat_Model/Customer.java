/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Satarat_Model;

/**
 *
 * @author lovel
 */
public class Customer {

    private int id;
    private String Name;
    private String Phone;
    private double Discount;
    private boolean status;

    public Customer(int id, String name, String phone, double Discount, boolean status) {
        this.id = id;
        this.Name = name;
        this.Phone = phone;
        this.Discount = Discount;
        this.status = status;
    }

    public Customer(int id, String name, String phone) {
        this.id = id;
        this.Name = name;
        this.Phone = phone;
    }

    public Customer(String name, String phone) {
        this.Name = name;
        this.Phone = phone;
        this.Discount = 0;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public void setDiscount(double Discount) {
        this.Discount = Discount;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", Name=" + Name + ", Phone=" + Phone + ", Discount=" + Discount + ", status=" + status + '}';
    }

}
