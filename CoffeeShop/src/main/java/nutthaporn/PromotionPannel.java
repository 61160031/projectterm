/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nutthaporn;

import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import nutthaporn_dao.PromotionDao;
import nutthaporn_model.Promotion_Model;

/**
 *
 * @author user
 */
public class PromotionPannel extends javax.swing.JPanel {

    private ArrayList<Promotion_Model> promostiontList;
    private PromotionTableModel model;
    Promotion_Model editedPromotiont;

    /**
     * Creates new form PromotionPannel
     */
    public PromotionPannel() {
        initComponents();
        PromotionDao dao = new PromotionDao();
        initForm();
        loadTable(dao);
    }

    private void initForm() {
        setFalseEnabled();
    }

    public void setFalseEnabled() {
        lblID.setEnabled(false);
        txtName.setEditable(false);
        txtStar.setEditable(false);
        txtEnd.setEditable(false);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        cmbStatusPro.setEnabled(false);
    }

    public void loadTable(PromotionDao dao) {
        promostiontList = dao.getAll();
        model = new PromotionTableModel(promostiontList);
        tblPromotion.setModel(model);
        //tblPromotion.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
        // @Override
        //public void valueChanged(ListSelectionEvent e) {
        //editedPromotiont = promostiontList.get(tblPromotion.getSelectedRow());
        // loadPromotionToForm();
        //}
        //});
    }

    private void loadPromotionToForm() {
        if (editedPromotiont.getId() >= 0) {
            lblID.setText("" + editedPromotiont.getId());
        }
        txtName.setText(editedPromotiont.getName());
        txtStar.setText(editedPromotiont.getStart());
        txtEnd.setText(editedPromotiont.getEnd());
        
        if(editedPromotiont.isStatus() == true){
            cmbStatusPro.setSelectedIndex(0);
        }else{
            cmbStatusPro.setSelectedIndex(1);
        }

        setTrueEnable();
    }

    public void setTrueEnable() {
        lblID.setEnabled(true);
        txtName.setEditable(true);
        txtStar.setEditable(true);
        txtEnd.setEditable(true);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);
        cmbStatusPro.setEnabled(true);
    }

    public void loadFormToPromotion() {
        editedPromotiont.setName(txtName.getText());
        editedPromotiont.setStart(txtStar.getText());
        editedPromotiont.setEnd(txtEnd.getText());
        
        int index = cmbStatusPro.getSelectedIndex();
        if (index == 0) {
            editedPromotiont.setStatus(true);
        } else {
            editedPromotiont.setStatus(false);
        }
    }

    public void refershTadle() {
        PromotionDao dao = new PromotionDao();
        ArrayList<Promotion_Model> newlist = dao.getAll();
        promostiontList.clear();
        promostiontList.addAll(newlist);
        tblPromotion.revalidate();
        tblPromotion.repaint();
    }

    public void clearEditeInForm() {
        editedPromotiont = null;
        lblID.setText("");
        txtName.setText("");
        txtStar.setText("");
        txtEnd.setText("");
        cmbStatusPro.setSelectedIndex(0);

        setFalseEnabled();
    }

    public boolean checkName() {
        if (!txtName.getText().equals("")) {
            return true;
        }
        UIManager.put("OptionPane.messageFont", new Font("Th-Chara", Font.PLAIN, 14));
        JOptionPane.showMessageDialog(this, "กรุณาพิมพ์ข้อมูลใหม่อีกครั้ง", "การบันทึกไม่สำเร็จ", JOptionPane.WARNING_MESSAGE);
        return false;
    }

    public boolean checkDate(String date) {
        if (date.length() == 10) {
            if (date.charAt(4) == '-' && date.charAt(7) == '-') {
                int year = Integer.parseInt(date.substring(0, 4));
                int month = Integer.parseInt(date.substring(5, 7));
                int day = Integer.parseInt(date.substring(8));
                if (year <= 2020 && year >= 2018) {
                    if (month <= 12 && month >= 1) {
                        if (day <= 31 && day >= 1) {
                            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                                if (day <= 31) {
                                    return true;
                                }
                            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                                if (day <= 30) {
                                    return true;
                                }
                            } else {
                                if (day <= 29) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                UIManager.put("OptionPane.messageFont", new Font("Th-Chara", Font.PLAIN, 14));
                JOptionPane.showMessageDialog(this, "กรุณาพิมพ์วันที่ให้ถูกต้อง", "การบันทึกไม่สำเร็จ", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        UIManager.put("OptionPane.messageFont", new Font("Th-Chara", Font.PLAIN, 14));
        JOptionPane.showMessageDialog(this, "กรุณาพิมพ์วันที่ตามรูปแบบนี้ 2020-01-01 ", "การบันทึกไม่สำเร็จ", JOptionPane.WARNING_MESSAGE);
        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblID = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtStar = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtEnd = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        cmbStatusPro = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPromotion = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();

        setBackground(new java.awt.Color(51, 110, 123));
        setMaximumSize(new java.awt.Dimension(860, 470));
        setMinimumSize(new java.awt.Dimension(860, 470));

        jPanel4.setBackground(new java.awt.Color(52, 73, 94));
        jPanel4.setMaximumSize(new java.awt.Dimension(148, 52));
        jPanel4.setMinimumSize(new java.awt.Dimension(148, 52));

        jLabel6.setBackground(new java.awt.Color(44, 62, 80));
        jLabel6.setFont(new java.awt.Font("TH-Chara", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("จัดการโปรโมชั่น");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE)
                    .addGap(149, 149, 149)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 77, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel1.setBackground(new java.awt.Color(51, 110, 123));

        btnSearch.setBackground(new java.awt.Color(34, 167, 240));
        btnSearch.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("ค้นหา");
        btnSearch.setMaximumSize(new java.awt.Dimension(77, 27));
        btnSearch.setMinimumSize(new java.awt.Dimension(77, 27));
        btnSearch.setPreferredSize(new java.awt.Dimension(77, 27));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(1, 152, 117));
        btnAdd.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnAdd.setText("เพิ่ม");
        btnAdd.setMaximumSize(new java.awt.Dimension(57, 27));
        btnAdd.setMinimumSize(new java.awt.Dimension(57, 27));
        btnAdd.setPreferredSize(new java.awt.Dimension(57, 27));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(248, 148, 6));
        btnEdit.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(255, 255, 255));
        btnEdit.setText("แก้ไข");
        btnEdit.setMaximumSize(new java.awt.Dimension(57, 27));
        btnEdit.setMinimumSize(new java.awt.Dimension(57, 27));
        btnEdit.setPreferredSize(new java.awt.Dimension(57, 27));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(192, 57, 43));
        btnDelete.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnDelete.setText("ลบ");
        btnDelete.setMaximumSize(new java.awt.Dimension(73, 27));
        btnDelete.setMinimumSize(new java.awt.Dimension(73, 27));
        btnDelete.setPreferredSize(new java.awt.Dimension(73, 27));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(44, 62, 80));
        jPanel3.setPreferredSize(new java.awt.Dimension(287, 305));

        jLabel2.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("รหัส :");

        lblID.setForeground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("ชื่อ :");

        txtName.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N

        jLabel4.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("วันที่เริ่ม :");

        txtStar.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N

        jLabel5.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("วันที่สิ้นสุด :");

        txtEnd.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N

        btnSave.setBackground(new java.awt.Color(34, 167, 240));
        btnSave.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("บันทึก");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(192, 57, 43));
        btnCancel.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("ยกเลิก");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("สถานะ :");

        cmbStatusPro.setFont(new java.awt.Font("TH-Chara", 0, 14)); // NOI18N
        cmbStatusPro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "อยู่ในช่วงโปร", "สิ้นสุดโปร" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel)
                        .addGap(20, 20, 20))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbStatusPro, 0, 185, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(9, 9, 9)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtStar)
                                    .addComponent(txtName)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEnd)))
                        .addGap(22, 22, 22))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                    .addComponent(lblID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(cmbStatusPro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnCancel))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        tblPromotion.setFont(new java.awt.Font("TH-Chara", 0, 16)); // NOI18N
        tblPromotion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "รหัสโปรโมชั่น", "ชื่อโปรโมชั่น", "วันที่เริ่ม", "วันสิ้นสุด", "สถานะ"
            }
        ));
        tblPromotion.setMaximumSize(new java.awt.Dimension(450, 240));
        tblPromotion.setMinimumSize(new java.awt.Dimension(450, 240));
        tblPromotion.setPreferredSize(new java.awt.Dimension(450, 240));
        jScrollPane1.setViewportView(tblPromotion);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 538, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(51, 110, 123));
        jPanel2.setPreferredSize(new java.awt.Dimension(835, 327));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 35, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 851, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        setTrueEnable();
        lblID.setText("0");
        editedPromotiont = new Promotion_Model(-1, "", "", "");
        loadFormToPromotion();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (checkName() && checkDate(txtStar.getText()) && checkDate(txtEnd.getText())) {
            loadFormToPromotion();
            PromotionDao dao = new PromotionDao();
            if (editedPromotiont.getId() >= 0) {
                dao.update(editedPromotiont);
            } else {
                dao.add(editedPromotiont);
            }
            refershTadle();
            clearEditeInForm();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearEditeInForm();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        if (tblPromotion.getSelectedRow() >= 0) {
            editedPromotiont = promostiontList.get(tblPromotion.getSelectedRow());
            loadPromotionToForm();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        if (tblPromotion.getSelectedRow() >= 0) {
            PromotionDao dao = new PromotionDao();
            editedPromotiont = promostiontList.get(tblPromotion.getSelectedRow());
            UIManager.put("OptionPane.messageFont", new Font("Th-Chara", Font.PLAIN, 14));
            int confrimDelete = JOptionPane.showConfirmDialog(null, "ยืนยันการลบโปรโมชั่น " + editedPromotiont.getId() + " " + editedPromotiont.getName(), "ยืนยันการลบ", JOptionPane.YES_NO_OPTION);
            if (confrimDelete == 0) {
                System.out.println(editedPromotiont.getName());
                dao.delete(editedPromotiont.getId());
                refershTadle();
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (!txtSearch.getText().equals("")) {
            ArrayList<Promotion_Model> promotionSerach = new ArrayList<>();
            for (Promotion_Model pro : promostiontList) {
                String name = pro.getName();
                if (name.contains(txtSearch.getText())) {
                    promotionSerach.add(pro);
                }
            }
            model = new PromotionTableModel(promotionSerach);
            tblPromotion.setModel(model);
        } else {
            UIManager.put("OptionPane.messageFont", new Font("Th-Chara", Font.PLAIN, 14));
            JOptionPane.showMessageDialog(this, "กรุณาพิมพ์คำค้นหาอีกครั้ง", "การค้นหาไม่สำเร็จ", JOptionPane.WARNING_MESSAGE);
            PromotionDao proDao = new PromotionDao();
            loadTable(proDao);
        }
    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox<String> cmbStatusPro;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblID;
    private javax.swing.JTable tblPromotion;
    private javax.swing.JTextField txtEnd;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtStar;
    // End of variables declaration//GEN-END:variables

    private class PromotionTableModel extends AbstractTableModel {

        private final ArrayList<Promotion_Model> data;
        String[] columName = {"ID", "Name", "Star", "End", "Status"};

        public PromotionTableModel(ArrayList<Promotion_Model> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 5;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Promotion_Model promotion = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return promotion.getId();
            }
            if (columnIndex == 1) {
                return promotion.getName();
            }
            if (columnIndex == 2) {
                return promotion.getStart();
            }
            if (columnIndex == 3) {
                return promotion.getEnd();
            }
            if (columnIndex == 4) {
                if (promotion.isStatus() == true) {
                    return "อยู่ในช่วงโปร";
                }
                return "สิ้นสุดโปร";
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columName[column];
        }

    }

}
